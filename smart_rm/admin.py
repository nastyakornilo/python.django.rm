# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from smart_rm.models import SmartBucketModel

admin.site.register(SmartBucketModel)
