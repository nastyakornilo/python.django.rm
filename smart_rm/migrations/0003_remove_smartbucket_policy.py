# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-19 20:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smart_rm', '0002_auto_20170619_2304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='smartbucket',
            name='policy',
        ),
    ]
