# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from smart_remover.utility.policy import AutocleanType

AUTOCLEAN_CHOICES = (
    (AutocleanType.old_files, AutocleanType.old_files),
    (AutocleanType.overflow, AutocleanType.overflow),
    (AutocleanType.combined, AutocleanType.combined),
)

class SmartBucketModel(models.Model):
    #name = models.CharField()
    user = models.ForeignKey('auth.User')
    dir = models.CharField(max_length=250)
    trashfiles_dir_name = models.CharField(blank=True, null=True, max_length=30)
    infofile_name = models.CharField(max_length=30, blank=True, null=True)
    max_size_in_bytes = models.IntegerField( blank=True, null=True)


    policy = models.ForeignKey('PolicyModel', on_delete=models.CASCADE, null=True)

    #text = models.TextField()
    # created_date = models.DateTimeField(
    #         default=timezone.now)
    # published_date = models.DateTimeField(
    #         blank=True, null=True)

    def publish(self):
        #self.published_date = timezone.now()
        self.save()

    def __str__(self):
        #return self.name
        return self.bucket_dir


class PolicyModel(models.Model):
    autoclean = models.CharField(max_length=20, choices=AUTOCLEAN_CHOICES, blank=True,
                                 default=AutocleanType.no_autoclean)

