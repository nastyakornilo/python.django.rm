from django import forms
from smart_rm.models import SmartBucketModel, PolicyModel



class BucketForm(forms.ModelForm):
    class Meta(object):
        model = SmartBucketModel
        exclude = ('policy', 'user')


class PolicyForm(forms.ModelForm):
    class Meta(object):
        model = PolicyModel
        fields = '__all__'
