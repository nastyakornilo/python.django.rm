# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils import timezone
from django.shortcuts import render
from smart_rm.models import SmartBucketModel
from django.shortcuts import get_object_or_404
from smart_rm.forms import BucketForm, PolicyForm
from django.shortcuts import redirect, HttpResponse
from smart_remover.lib.smart_bucket import SmartBucket


def bucket_list(request):
    buckets = SmartBucketModel.objects.all() #filter(published_date__lte=timezone.now()) #.order_by('published_date')
    return render(request, 'smart_rm/buckets_list.html', {'buckets': buckets})


def bucket_detail(request, pk):
    bucket_model = get_object_or_404(SmartBucketModel, pk=pk)
    smart_bucket = SmartBucket(bucket_model.dir)
    #bucket_items = smart_bucket.show_content()
    return render(request, 'smart_rm/bucket_detail.html', {'bucket': bucket_model, 'bucket_items': smart_bucket.show_content(None, None)})

def bucket_new(request):
    if request.method == "POST":
        bucket_form = BucketForm(request.POST)
        policy_form = PolicyForm(request.POST)
        if bucket_form.is_valid() and policy_form.is_valid():
            bucket = bucket_form.save(commit=False)
            bucket.user = request.user
            policy = policy_form.save()
            bucket.policy = policy
            #bucket.published_date = timezone.now()
            bucket.save()
            return redirect('bucket_detail', pk=bucket.pk)
    else:
        bucket_form = BucketForm()
        policy_form = PolicyForm()
    return render(request, 'smart_rm/bucket_edit.html', {'bucket_form': bucket_form, 'policy_form':policy_form})


def bucket_edit(request, pk):
    bucket = get_object_or_404(SmartBucketModel, pk=pk)
    if request.method == "POST":
        bucket_form = BucketForm(request.POST, instance=bucket)
        policy_form = PolicyForm()
        #policy_form = PolicyForm(request.Post, instance=bucket.policy)
        if bucket_form.is_valid() and policy_form.is_valid():
            bucket = bucket_form.save(commit=False)
            policy = policy_form.save()
            #bucket.author = request.user
            #bucket.published_date = timezone.now()
            bucket.policy = policy
            bucket.save()
            return redirect('bucket_detail', pk=bucket.pk)
    else:
        bucket_form = BucketForm(instance=bucket)
        policy_form = PolicyForm(instance=bucket.policy)
    return render(request, 'smart_rm/bucket_edit.html', {'bucket_form': bucket_form, 'policy_form': policy_form})


def clear(request, pk, index):
    bucket_model = get_object_or_404(SmartBucketModel, pk=pk)
    smart_bucket = SmartBucket(bucket_model.dir)
    if request.method == "POST":
        smart_bucket.clear_by_index(int(index))
    return redirect('bucket_detail', pk=pk)


def add(request, pk):
    bucket_model = get_object_or_404(SmartBucketModel, pk=pk)
    smart_bucket = SmartBucket(bucket_model.dir)
    if request.method == "POST":
        smart_bucket.remove(request.POST['path'])
    return redirect('bucket_detail', pk=pk)


def restore(request, pk, index):
    bucket_model = get_object_or_404(SmartBucketModel, pk=pk)
    smart_bucket = SmartBucket(bucket_model.dir)
    if request.method == "POST":
        smart_bucket.restore_by_index(int(index))
    return redirect('bucket_detail', pk=pk)