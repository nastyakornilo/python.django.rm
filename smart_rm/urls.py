from django.conf.urls import url, include
from smart_rm import views

urlpatterns = [
    url(r'^$', views.bucket_list, name='buckets_list'),
    url(r'^bucket/(?P<pk>[0-9]+)/$', views.bucket_detail, name='bucket_detail'),
    url(r'^bucket/(?P<pk>[0-9]+)/clear/(?P<index>[0-9]+)/$', views.clear, name='clear'),
    url(r'^bucket/(?P<pk>[0-9]+)/add/$', views.add, name='add'),
    url(r'^bucket/(?P<pk>[0-9]+)/restore/(?P<index>[0-9]+)/$', views.restore, name='restore'),
    url(r'^bucket/new/$', views.bucket_new, name='bucket_new'),
    url(r'^bucket/(?P<pk>[0-9]+)/edit/$', views.bucket_edit, name='bucket_edit'),
    # url(r'^buckets_tasks/$', views.buckets_tasks, name='bucket_edit'),
]